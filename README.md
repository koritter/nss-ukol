# TaskTrack

## Authors
- Jana Nováková
- Petr Novotný

## Technologies Used
- Node.js
- Express.js
- MongoDB
- React.js

## Description
This project is a simple web application used for task management. Users can create accounts, add tasks, edit them, and mark them as completed.

## Installation Instructions
To install and run the application, follow these steps:
1. Clone this repository to your local environment.
2. Install the necessary dependencies using the command `npm install`.
3. Run the application using the command `npm start`.

## Copyright
© 2024 Jana Nováková, Petr Novotný. All rights reserved.


